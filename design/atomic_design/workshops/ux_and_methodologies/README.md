# UX and Methodologies

![ux_and_methodologies_header](https://gitlab.com/opentooladd/design/atomic_design/-/wikis/uploads/1c7a793e7ca03cebaad99b5283547231/ux_and_methodologies_header.png)

# Table of Content

+ [Lean UX and Design Thinking](#lean-ux-and-design-thinking)
   + [A method for every Contexts](#a-method-for-every-contexts)
   + [Entry Point: The User](#entry-point%3A-the-user)
      + [Exercise](#empathy-exercise)
   + [Define the solutions](#define-the-solutions)
      + [User Scenarios](#user-scenarios)
      + [Exercise](#define-exercise)
   + [Sketches](#sketches)
      + [Exercise](#sketches-exercise)
   + [Decisions](#decisions)
      + [Exercise](#decisions-exercise)
   + [Prototype](#prototype)
   + [Validation](#validation)
+ [Conclusion](#conclusion)
+ [Appendix](#appendix)
+ [Links](#links)

## Lean UX and [Design Thinking](https://en.wikipedia.org/wiki/Design_thinking)

> "Design thinking refers to the cognitive, strategic and practical processes by which design concepts (proposals for new products, buildings, machines, etc.) are developed. Many of the key concepts and aspects of design thinking have been identified through studies, across different design domains, of design cognition and design activity in both laboratory and natural contexts."<br>Wikipedia, *Design Thinking*, https://en.wikipedia.org/wiki/Design_thinking 

### A method for every Contexts

First things first, Design Thinking is a method amongst others and isn't the *perfect* one for every project. Each context and project should have a tailored methodology adapted to its needs. What I propose with this workshop is to learn about a specific method called Design Thinking and some of the processes it introduces.

Why ?<br>
The keywords upon which this method is founded are **Empathy, Definition, Creation, Prototyping and Tests**. I strongly believe that these keyword well align with our values and idea of what is a project methodology.

To illustrate that, I suggest that we begin by going though the first keyword: **Empathy**, and what best way to do that than to analyze our users ?

### Entry Point: The User

> In this step should participate everybody with a potential use case or edge case in mind. Generally there is one or more ProductOwner, one or more Designers, one Techinician (because technique can imply some scenario that we can't account for; for example the different steps in credit card payment are different and are best known by the one implementing them).

Unless we are creating a project for ourselves (in this case we are our only users), the goal of a project is to create and / or work on a product<sup>[(1)](#anchor-1)</sup> that resolves concrete **needs**. These **needs** are produced and expressed by our end users.

They could be everything, from people to machines going through animals and vegetals. But we are going to focus on people for this workshop.

There are multiple ways to define what users needs (cf: [Link](#links)), but what we can begin to do as we are one of the potential users of [Civil-Add](https://gitlab.com/opentooladd/civil-add), its to **project** ourselves in the final users and imagine what we would need the final product to do.

On of the common way of translating this mental process is the creation of **personas**<sup>[(2)](#anchor-2)</sup> that describe your users and that you can use in various scenarios.<br>
The interest of these personas are to artificially represent some the users that will interact with the final product.

For example, we can have Gilbert Montagné, wannabee journalist, 24 year, in school, 600 to 900 euros of incomes, wants to find someone to help him create a website to promote his articles.

What does he need ?
What are his potential possibilities ? What will he potentially choose and why ?
What are his assets ? What does he possess we can potentially exploit to provide solutions ?

> :exclamation: This step is the good one to create concept ideas of what would be the best responses for the needs of our users. There commonly are multiple responses to one need, and they are pondered by physical, intellectual and economical resources. Sometime the *de facto* solution isn't the best one for given set of users and your product could be separated in multiple smaller products fitting specifical needs for different users.<br>Have you thought that your end users may not be thrilled about using computers ?<br>Have you thought that your end users may not be connected all the time to the web ?

#### Empathy - Exercise

1) Imagine 2-3 personas that could be some end users for your product. Base them on yourselves for example !

### Define the solutions

> This step is generally done with the people most suited to express the needs in term of sprint goals. So there can be one or more ProductOwner. One or more Designer. One or more ProductManager.

This step is to evaluate everything that have been learned in the precedent phase to establish focus. This is done by defining specific context and desired outcomes of potential solutions.

Now that we know what Gilbert and his friends needs, and that we have a clear view of the needs to resolve, we can begin to imagine solutions to these needs.
Here we take the assets and the possibilities of every personas we have in store and join them to extract the similarities. This will help define the more versatil solutions for our users.

It is generally at this phase that we can begin to establish what we call **User Scenarios**.

#### User Scenarios

The goal of a User Scenario is to represent a set of steps through which a given user will pass to realize one of his needs.
Each of these steps are composed of **word** that provide specific meaning given a certain context. There are some [lexic](https://cucumber.io/docs/gherkin/reference/#keywords) that are wildly used and that we can also reuse for the sake of this workshop. But know then that you are totally allowed to create some lexicology more suited to your need.

For example, a User Scenario could be something like that:

> **Given** I am an **Administrator**<br>
**And** I am on the page *Home*<br>
I can access my *Account* page through the *Account* link in the navigation

These scenarios will then be handled to other members of the team, typically UI Designer, or Graphic Designers (depends on how you call them) to create prototypes.

#### Define - Exercise

1) Take some of your personas and create some User Scenario using the Gherkin langage. Try to by exhaustive and to take in account the caracteristic of each persona.

### Sketches

> This step is generally done by the UI / UX designers or the one capable of putting wireframes in place. It is generally at this stage that you will begin to create your Atomic Design Pattern-Lab.

At this step it is time to take into account what are the devices that the end user will be using and extrapolate what elements of UI does he need to interact with the product.

Take in your favorite Vector Drawing Software or whatever you like to draw (even paper, it doesn't matter, its a prototype phase !) and start designing some wireframes.
Imagine the input, imagine the button, imagine the knobs, the logo, the text, etc... Everything that you feel you're end user will need to have.

Don't hesitate to include animations, storyboards, everything you need to convey the meaning you need.

> :exclamation: You can begin to separate into atoms, and molecules, etc... but don't be to picky, because at this stage there will be some back and forth with the product team and some users to decide what will be the good sketches. So lexicology will shift and there are high chance that what is a molecule at this point will maybe become an organism after.

#### Sketches - Exercise

1) Take some of your User Scenarios and try creating some sketches that correspond. They don't have to be complicated, simple rectangles, circles and text will suffise. Of course if you want to do more, fill free to do more.

### Decisions

> This stage is the back and forth phase with the users and the product team to determine what are the best solutions. At the end there should be 1 solution for every scenario given.

This is the time to make decisions...
It is interesting to go through a phase of interactive prototyping to be able to do some rapid prototype testing on end-users and product team. But don't mobilize a dev team for that (except if you can), it is agreable to be able to do it, but not necessary.
We eliminate the sketches that don't fit the needs totally and if it misses a responses, there are 2 solutions.

First we can try another prototyping iteration to see if there are other solutions to imagine. Maybe it is just something we forgot ?<br>
Or we can go through another Definition phase to refine the User Scenarios.

The choice depends on the availability of the teams and their ability to produce more content. If there isn't any solution that are viable, maybe there is a problem with the base analysis of the problem and you should go back to the first phase ?<br>Unless the needs weren't sufficiently specified and understood, there shouldn't be any need to go through all the process.

As an end product of this phase you should have a set of skecthes that respond to given User Scenarios that you can include in a Sprint to actually do.

#### Decisions - Exercise

1) Choose between your sketches and decide which one best suits you users. Take into account every step of the corresponding User Scenario and see if it fits well.

### Prototype

> This step is generally done by the Designers, or Developpers, depends on who is capable of doing it the fastest and more easily.

At this stage it is time to handle the User Scenarios and their corresponding sketches to the prototyping team.
They will create some live examples of the scenarios for your users to interact with (it could have been mingled with the first phase to, but it is a bonus, this one is the real one).

There should be a clear design lab established with a pretty decent Design System with some atoms, molecules, etc... available.

At this point your prototyping team basically has to fill the abstract values with real ones, choose the colors, choose the font, create the style, graphic design amd implement the code for the product by referencing themselves to the wireframes and User Scenarios.

### Validation

> This step is done with some end users and the product team. Some other people can be involved in the process to judge the final prototypes.

There should be some back and forth with the prototyping team and some end users and Product team to validate that the prototypes correctly implements the User Scenarios and sketches prestablished.

Be careful the the detail, and be careful to the insight of your end user. But don't forget that he doesn't have every insight on his end.

## Conclusion

These are the steps defined by Design Thinking to go through a creative process from beginning to end.

Of course, this is one of the many interpretations of the concepts stablished by Design Thinking concepts. There are multiple resources (some are listed in the [Links Sction](#links)) that define there own methodologies based on these concepts, some of them I have extensively reused in this workshop.

Nonetheless, even if the specific method presented here isn't the best for you, I highly encourage you to go through the links and to establish your own methodology that best fits your needs.

## Appendix

+ #### (1)
> **product**: as the [CNRTL]() states:<br>
"Result of one's mind's creative activity or sensitivity"<br>
CNRTL, *Produit*, https://www.cnrtl.fr/definition/produit/substantif<br>
This can be extended to the following, which I use in the rest of this document: Result a group or single individual's creativity or sensitivity created to respond to the needs of peers and / or other individuals and / or entities.

+ #### (2)
> **personas**: Stereotical caracters having a certain set of attributes to define them<br>

## Links

+ English
+ [List of UX methodologies](https://uxplanet.org/most-common-ux-design-methods-and-techniques-c9a9fdc25a1e)
+ [Stanford Methodologies By Google](https://designsprintkit.withgoogle.com/methodology/overview)
+ [Design Concil Website](https://www.designcouncil.org.uk/news-opinion/what-framework-innovation-design-councils-evolved-double-diamond)
+ French
+ [Lean UX Processes](https://www.quatreplusquatre.com/methodologies-experience-utilisateur-design-UX.html)
+ [Design Thinking](https://www.quatreplusquatre.com/lean-UX-et-design-thinking.html#design-thinking)
+ [Lean UX](https://www.quatreplusquatre.com/lean-UX-et-design-thinking.html#lean-ux)
+ [Templates and Resources](https://designsprintkit.withgoogle.com/resources/overview)
