# Atomic Design Basic

![atomic_pattern_global](https://gitlab.com/opentooladd/design/atomic_design/-/wikis/uploads/c26832460dc8284d1ddc5db2e65f17a6/atomic_pattern_global.png)

This is the first workshop on Atomic Design that will cover the basic building blocks of the methodology as described by Brad Frost in is book.

First, we will studies the structure and abstract models we will use to reflect about our interfaces and products.

Then we will create what is called a *design atmosphere* or *[content inventory](https://en.wikipedia.org/wiki/Content_inventory)* by visually referencing every piece of **UI**<sup>[(1)](#anchor-1)</sup> available on a given product (let's say, Tool-Add's website for example ;))

And finally, we will extend our work and thougths by applying what we have learned to a new product that will be created by the same company, [Civil-Add](https://gitlab.com/opentooladd/civil-add). We will try to expand the *design atmosphere* precently created with the website, and create prototype **style guide**<sup>[(2)](#anchor-2)</sup> for a given scenarios.

## Table of Content

+ [Principles and chemistry](#principles-and-chemistry)
  + [Atoms](#atoms)
    + [Exercise](#atoms-exercise)
  + [Molecules](#molecules)
    + [Exercise](#molecules-exercise)
  + [Organisms](#organisms)
    + [Exercise](#organisms-exercise)
+ [Modularity and Reusability](#modularity-and-reusability)
  + [Templates](#templates)
    + [Exercise](#templates-exercise)
  + [Pages](#pages)
    + [Exercise](#pages-exercise)
+ [Conclusion](#conclusion)
    + [Exercise](#conclusion-exercise)
+ [Tools](#tools)
+ [Appendix](#appendix)
+ [Links](#links-and-articles)


## Principles and chemistry

> "Chemical reactions are represented by chemical equations, which often show how atomic elements combine together to form molecules. [...] In the natural world, atomic elements combine together to form molecules. These molecules can combine further to form relatively complex organisms."<br>Brad Frost, *Atomic Design*, 2020, Brad Frost, p 40

Like Brad exposes it well in is book: *Atomic design is a methodology composed of five distinct stages working together to create interface design systems in a more deliberate and hierarchical manner*.
Has atoms of matters combine to form living organisms as complex as the Human, we can reuse this model to build large scale systems and complex interfaces.

> "Atomic design is not a linear process, but rather a mental model to help us think of our user interfaces as both a cohesive whole and a collection of parts at the same time. Each of the five stages plays a key role in the hierarchy of our interface design systems."<br>Brad Frost, *Atomic Design*, 2020, Brad Frost, p 42

Through this part we will explain each building blocks and provide concrete examples through exercises.

### Atoms

![atomic_pattern_atoms](https://gitlab.com/opentooladd/design/atomic_design/-/wikis/uploads/8e64f414027735faf9bf8e4b6db66f87/atomic_pattern_atoms.png)

> "**Atoms** are the basic building blocks of all matter. Each chemical element has distinct properties, and they can’t be broken down further without losing their meaning. (Yes, it’s true atoms are composed of even smaller bits like protons, electrons, and neutrons, but atoms are the smallest *functional* unit.)"<br>Brad Frost, *Atomic Design*, 2020, Brad Frost, p 40

If atoms are the basic building blocks of matter, then the atoms of our interfaces serve as the foundational building blocks that comprise all our user interfaces.

To reuse our examples, in the context of a website or webapp, atoms could be **all the [ basic HTML blocks ](https://developer.mozilla.org/en-US/docs/Web/HTML/Element)** like form, labels, inputs, paragraphes, etc...
In the context of a machine, it can be more granular, because lots of controls are already molecules, but atoms could be screws, metal bars, springs, etc...
These are juste practical examples, but it can be applied to a large variety of contexts.

![atoms_example](https://gitlab.com/opentooladd/design/atomic_design/-/wikis/uploads/c5e085f05d824e1b67348221325e0871/atoms_example.png)

These can't be broken down without losing there meaning in the context we defined.<br>
An HTML paragraphe would be juste basic character in a certain place in a hierarchy.<br>
A screw or a spring is just a piece of metal with a defined shape and usage.<br>
We can go further with breaking down, but it doesn't have any purpose in our contexts.

> :exclamation: This methodology heavily relies upon categorization, which means creating artificial / abstract boundaries to contain groups of objects with common attributes. These boundaries can and should be broken and rethought for the system to be easily scalable and modular. To do this, it is important to have a solid and concrete lexicology or understanding of the goals of the project, in order to set appropriate boundaries and definitions at a concrete time.

#### Atoms - Exercise

1) Go to [Framasoft's website](https://framasoft.org/)
2) Go through the home page and take a screenshot of all of what you consider to be atoms and paste them in a new page that you created in a presentation tool of you choice (see [Tools](#tools) for some ideas)

### Molecules

![atomic_pattern_molecules](https://gitlab.com/opentooladd/design/atomic_design/-/wikis/uploads/b70d6fe1910e09978c2b31199f6e0dc2/atomic_pattern_molecules.png)

> "**Molecules** are groups of two or more atoms held together by chemical bonds. These combinations of **atoms** take on their own unique properties, and become more tangible and operational than atoms."<br>Brad Frost, *Atomic Design*, 2020, Brad Frost, p 40 

In interfaces, molecules are **relatively simple groups of UI elements functioning together as a unit**. For example, a form label, search input, and button can join together to create a search form molecule.

When combined, these abstract atoms suddenly have purpose. The label atom now defines the input atom. Clicking the button atom now submits the form. The result is a simple, portable, reusable component that can be dropped in anywhere search functionality is needed.

![molecules_example](https://gitlab.com/opentooladd/design/atomic_design/-/wikis/uploads/92cfca97ccd9874c3626a3ecc244edbd/molecules_example.png) 

In the example of the machine, it is this real that we will begin to assemble screws, springs and plastique knobs into a functionning button lever or individual microphones.

Now, assembling elements into simple functioning groups is something we’ve always done to construct user interfaces. But dedicating a stage in the atomic design methodology to these relatively simple components affords us a few key insights.

Creating simple components helps UI designers and developers adhere to the [ single responsibility principle ](https://en.wikipedia.org/wiki/Single_responsibility_principle), an age-old computer science precept that encourages a “do one thing and do it well” mentality. Burdening a single pattern with too much complexity makes software unwieldy. Therefore, creating simple UI molecules makes testing easier, encourages reusability, and promotes consistency throughout the interface.

Now we have simple, functional, reusable components that we can put into a broader context.

Enter organisms!

#### Molecules - Exercise

1) Go to [Framasoft's website](https://framasoft.org/)
2) Go through the home page and take a screenshot of all of what you consider to be molecules and paste them in a new page that you created in a presentation tool of you choice (see [Tools](#tools) for some ideas)
3) Emphasize on the Molecules using Atoms you precendtly found

### Organisms

![atomic_pattern_organism](https://gitlab.com/opentooladd/design/atomic_design/-/wikis/uploads/fb8578775c0423c9a8bad3016b91cfa6/atomic_pattern_organism.png)

> "**Organisms** are assemblies of **molecules** functioning together as a unit. These relatively complex structures can range from single-celled organisms all the way up to incredibly sophisticated organisms like human beings."<br>Brad Frost, *Atomic Design*, 2020, Brad Frost, p 40 

Organisms are relatively complex UI components composed of groups of molecules and/or atoms and/or other organisms. These organisms form distinct sections of an interface.

For example, a search form can often be found in the header of many web experiences, so let’s put that search form molecule into the context of a header organism.

![organisms_example](https://gitlab.com/opentooladd/design/atomic_design/-/wikis/uploads/bacd67418ba31ab1a0b547f0dd60da4d/organisms_example.png)

This header organism is composed of a search form molecule, logo atom, and primary navigation molecule. The header forms a standalone section of an interface, even though it contains several smaller pieces of interface with their own unique properties and functionality.

Organisms can consist of similar or different molecule types. A header organism might consist of dissimilar elements such as a logo image, primary navigation list, and search form. We see these types of organisms on almost every website we visit.

-----

While some organisms might consist of different types of molecules, other organisms might consist of the same molecule repeated over and over again. For instance, visit a category page of almost any e-commerce website and you’ll see a listing of products displayed in some form of grid.

Building up from molecules to more elaborate organisms provides designers and developers with an important sense of context. Organisms demonstrate those smaller, simpler components in action and serve as distinct patterns that can be used again and again. The product grid organism can be employed anywhere a group of products needs to be displayed, from category listings to search results to related products.

Now that we have organisms defined in our design system, we can break our chemistry analogy and apply all these components to something that resembles a web page

#### Organisms - Exercise

1) Go to [Framasoft's website](https://framasoft.org/)
2) Go through the home page and take a screenshot of all of what you consider to be organisms and paste them in a new page that you created in a presentation tool of you choice (see [Tools](#tools) for some ideas)
3) Emphasize on the Organisms using Molecules and Atoms you precendtly found

## Modularity and Reusability

It is here that we leave our analogy with the chenmistry world.
On this section i will emphasize on web interface and application design, but keep in mind that this process is already used is some extent in every factory process in the form of blueprints and molds.

### Templates

![atomic_pattern_templates](https://gitlab.com/opentooladd/design/atomic_design/-/wikis/uploads/5355fa4860f0fe550222f991627c71de/atomic_pattern_templates.png)

> "**Templates are page-level objects that place components into a layout and articulate the design’s underlying content structure**. To build on our previous example, we can take the header organism and apply it to a homepage template."<br>Brad Frost, *Atomic Design*, 2020, Brad Frost, p 49

Another important characteristic of templates is that they focus on the page’s underlying content structure rather than the page’s final content. Design systems must account for the dynamic nature of content, so it’s very helpful to articulate important properties of components like image sizes and character lengths for headings and text passages.

![organisms_header](https://gitlab.com/opentooladd/design/atomic_design/-/wikis/uploads/f53cc67ef6ccbb39c552bf306a473dbf/organisms_header.png)

[ Mark Boulton ](https://markboulton.co.uk/) discusses the importance of defining the underlying content structure of a page:

> "You can create good experiences without knowing the content. What you can’t do is create good experiences without knowing your content structure. What is your content made from, not what your content is."<br>Mark Boulton

By defining a page’s skeleton we’re able to create a system that can account for a variety of dynamic content, all while providing needed guardrails for the types of content that populate certain design patterns. For example, the homepage template for Time Inc. shows a few key components in action while also demonstrating content structure regarding image sizes and character lengths:

![templates_example](https://gitlab.com/opentooladd/design/atomic_design/-/wikis/uploads/e662cf18754c994617b409efe2143eef/templates_example.png)

#### Templates - Exercise

Now that we have some atoms, molecules and organisms, we can create a template to actually structure our content.

1) Go to [Framasoft's website](https://framasoft.org/)
2) Based on your precedent research, rethink the structure of the page. The goal is to keep the message, but rethink the way it is displayed.

### Pages

![atomic_pattern_pages](https://gitlab.com/opentooladd/design/atomic_design/-/wikis/uploads/05a40a00097117569817a23afd60c43e/atomic_pattern_pages.png)

**Pages are specific instances of templates that show what a UI looks like with real representative content in place**. Building on our previous example, we can take the homepage template and pour representative text, images, and media into the template to show real content in action.

![pages_example_1](https://gitlab.com/opentooladd/design/atomic_design/-/wikis/uploads/b4e7211a74ca2d83050ec282cc7dec89/pages_example_1.png)

In addition to demonstrating the final interface as your users will see it, **pages are essential for testing the effectiveness of the underlying design system**. It is at the page stage that we’re able to take a look at how all those patterns hold up when real content is applied to the design system. Does everything look great and function as it should? If the answer is no, then we can loop back and modify our molecules, organisms, and templates to better address our content’s needs.

![pages_example_2](https://gitlab.com/opentooladd/design/atomic_design/-/wikis/uploads/cc74bc9512c0a7f494a27bba8ff7e191/pages_example_2.png)

**Pages also provide a place to articulate variations in templates**, which is crucial for establishing robust and reliant design systems. Here are just a few examples of template variations:

  + A user has one item in their shopping cart and another user has ten items in their cart.
  + A web app’s dashboard typically shows recent activity, but that section is suppressed for first-time users.
  + One article headline might be 40 characters long, while another article headline might be 340 characters long.
  + Users with administrative privileges might see additional buttons and options on their dashboard compared to users who aren’t admins.

In all of these examples, the underlying templates are the same, but the user interfaces change to reflect the dynamic nature of the content. These variations directly influence how the underlying molecules, organisms, and templates are constructed. Therefore, creating pages that account for these variations helps us create more resilient design systems.

#### Pages - Exercise

Now that we have some templates, we have to fill these with appropriate content.

1) Go to [Framasoft's website](https://framasoft.org/)
2) Extract the image there or take screenshot (it doesn't have to be clean) and fill the template you precendtly created with real content. Try to categorize your content. Why have you use this image more than another ? What makes it effective ?

### Conclusion

So that’s atomic design! These five distinct stages concurrently work together to produce effective user interface design systems. To sum up atomic design in a nutshell: 
+ **Atoms** are UI elements that can’t be broken down any further and serve as the elemental building blocks of an interface.
+ **Molecules** are collections of atoms that form relatively simple UI components.
+ **Organisms** are relatively complex components that form discrete sections of an interface.
+ **Templates** place components within a layout and demonstrate the design’s underlying content structure.
+ **Pages** apply real content to templates and articulate variations to demonstrate the final UI and test the resilience of the design system.

![conclusion_pattern](https://gitlab.com/opentooladd/design/atomic_design/-/wikis/uploads/2339ed33044c788b4fec5f7fa93f61e4/conclusion_pattern.png)

**One of the biggest advantages atomic design provides is the ability to quickly shift between abstract and concrete**. We can simultaneously see our interfaces broken down to their atomic elements and also see how those elements combine together to form our final experiences.

When designers and developers are crafting a particular component, we are like the painter at the canvas creating detailed strokes. When we are viewing those components in the context of a layout with real representative content in place, we are like the painter several feet back from the canvas assessing how their detailed strokes affect the whole composition. It’s necessary to zero in on one particular component to ensure it is functional, usable, and beautiful. But it’s also necessary to ensure that component is functional, usable, and beautiful in the context of the final UI.

Atomic design provides us a structure to navigate between the parts and the whole of our UIs, which is why it’s crucial to reiterate that **atomic design is not a linear process**. It would be foolish to design buttons and other elements in isolation, then cross our fingers and hope everything comes together to form a cohesive whole. So don’t interpret the five stages of atomic design as “Step 1: atoms; Step 2: molecules; Step 3: organisms; Step 4: templates; Step 5: pages.” **Instead, think of the stages of atomic design as a mental model that allows us to concurrently create final UIs and their underlying design systems**.

### Conclusion - Exercise

You now have all the knowledge needed to effectively use and think in terms of atomic design. So let's us this knowledge to actually build something that is usefull for some of Open Tool-Add's project !

To do that, we will use what is called [User Experience](https://en.wikipedia.org/wiki/User_experience_design) or [UX](https://en.wikipedia.org/wiki/User_experience_design) and [User scenarios](https://www.interaction-design.org/literature/topics/user-scenarios) to provide us with a concrete use case to build our first atoms, molecules and organisms.

Here there are 2 solutions to begin the exercise. 

You can follow on the workshop and go to [UX and Methodologies](UX-and-Methodologies) to learn about user experience and how to create effective scenarios based on your use cases.

Or you can use the prebuild scenario i will write down here.

If you already know how to create user scenarios, i strongly encourage you to build your own scenarios and craft some atoms from there.

> As a **User**, i want to be able to create an account on Civil-Add software.<br>
**Given** i am on the page `/account/create`<br><br>
-- I **See** a **Create Account Form oraganism** *with* *3* **Form Input Molecules** composed of:<br>
--------------------
> ---- 1 *label atom* **with a value** of: "Username"<br>
---- 1 *input atom* **with type** "text" and **with a placeholder value** of: "Username"<br>
---- 1 *error message atom* **with a value** of: "no user with this username"<br><br>
---- 1 *label atom* **with a value** of: "password"<br>
---- 1 *input atom* **with type** "password" and **with a placeholder value** of: "Username"<br>
---- 1 *error message atom* **with a value** of: "no user with this username"<br><br>
---- 1 *label atom* **with a value** of: "email"<br>
---- 1 *input atom* **with type** "email" and **with a placeholder value** of: "Email"<br>
---- 1 *error message atom* **with a value** of: "This is not a valid email"<br>
------
> -- **And** 1 *button atom* **with a value** of: "Submit"<br>
---- **When** I *click* **And** the values *Username*, *Email*, *Password* **Respect**:<br>
\------- *Username*: *length* < 32<br> 
\------- *Password*: *regex* /[A-Z]{1,}[0-9]{1,}[\!-\?-\<-\>-\.-\/\\]{1,}/<br> 
\------- *Email*: *regex* /\w+@\w+\.\w+/<br> 
---- **Then** a `POST` request is sent to the server with the values *Username*, *Email*, *Password*
------
> -- **Given** it is a *Success*<br>
---- **Then** I **See** a *Success Message Box organism* **with a value** of: "Your account has successfully been created. You are now going to be redirected to the Login page"<br>
------
> -- **Given** it is a *Failure*<br>
---- **Then** I **See** an *Error Message Box organism* **with a value** of: "Oups, there was a problem on our end...<br> Please ty again later"

### Tools

List of tools that you will need to perform this workshop:

- Presentation tools
  - **Windows**
    - Powerpoint *proprietary*<br>
    <em>If you have a Windows computer, you certainly have this softare installed. Try searhing for Powerpoint in the dock searchbar</em>
  - **Mac**
    - Pages *proprietary*<br>
    <em>Default MacOS presentation tools. You should have it in the dock, else search for it with the searchbar</em>
  - **Windows, Mac, Linux**
    - [Libre Office Impress](https://www.libreoffice.org/download/download/) *Open Source*<br>
    <em>Depends of your distro, but you could have it installed. If not, try to install it with your package manager prior to downloading it</em>
- Vector drawing tools
  - **Windows**
    - [Adobe Illustrator](https://www.adobe.com/fr/products/illustrator.html) *proprietary*
  - **MacOS**
    - [Adobe Illustrator](https://www.adobe.com/fr/products/illustrator.html) *proprietary*
    - [Sketch](https://www.sketch.com/) *proprietary*
  - **Windows, Mac, Linux**
    - [Inkscape](https://inkscape.org/release/inkscape-0.92.4/) *Open Source*
    - [Vectr](https://vectr.com/) *proprietary but online version*
- Digital Composition
  - **Windows, Mac, Linux**
    - [Gimp](https://www.gimp.org/)<br>
    <em>You could use that as a replacement for all the others, but it needs bit of knowledge to do everything smoothly. If you are not experiences, i suggest to use the dedicated tools</em>

- Screenshot taking tools<br>
<em>Screenshots tools relies on windowing systems. The major ones (Gnome, Xfce...) ships their own tool wich are good. Otherwise you can install them or others.</em>
  - **Window**
    - Windows has a native screenshot tool that's fairly good. Juste search *screenshot* in the searchbar.
  - **Linux**
    - [Shutter](https://shutter-project.org/downloads/)
    - [Xfce-Screenshot](https://xfce.org/download)

### Appendix

+ #### (1)
> **UI**: Acronyme for **User Interface**.<br>
A User interface is everything needed for a product user to interact and use the product.<br>
In the case of screen application, it consists of every visual elements displayed on the screen that allows the user to manipulate the whole or a part of the product.<br>
In the case of a machine, it consists of every piece of buttons, levers, screens, etc... needed for the person who will use the machine to actually understand and interact with it.
+ #### (2)
> **style guide**: "...document and organize design materials while providing guideline usage, and guardrails."<br>Brad Frost, *Atomic Design*, 2020, Brad Frost, p23<br><br>
Examples of styleguides:<br>
+ [Material Design](https://material.io/design/introduction/#)
+ [Github Styleguide](https://github.com/styleguide/)
+ [Yelp Styleguide](https://www.yelp.com/styleguide)
+ [Mailchimp Patterns](http://ux.mailchimp.com/patterns/color)
+ [Styleguide examples](http://styleguides.io/examples.html)

### Links and Articles

+ [atomic design workflow](https://atomicdesign.bradfrost.com/chapter-4/#frontend-prep-chef)
+ [User scenarios](https://www.interaction-design.org/literature/topics/user-scenarios)
+ [User Experience](https://en.wikipedia.org/wiki/User_experience_design)
+ [Gherkin langage Reference](https://cucumber.io/docs/gherkin/reference/#keywords)
