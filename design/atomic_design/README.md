# Atomic Design

This projet is about [Atomic Design](https://atomicdesign.bradfrost.com/), which is a design methodology oriented around
modularity, deconsctruction and composition.

I strongly encourage you ro read the book [Atomic Design By Brad Frost](https://atomicdesign.bradfrost.com/table-of-contents/)(which is free by the way).
Otherwise i bought it, and put it in the `/resources` folder. Feel free to browse, but if you really like it, consider paying for it (it is 10€) :)

## Table Of Content

 + [Goal](#goal-2)
 + [Structure](#structure)
 + **Pages**
   + [Atomic Design Basics](Atomic-Design-Basics)

## Goal

The goal of this repo is to contain assets and resources for atomic design.
It is also the entry point for all people who wants to learn about our Atomic Design principles and methodologies.

## Structure

+ `/resources`
> All the resources needed for the documentation in this repo. If it wasn't possible to download the asset, there is a link.
+ `/workshops`
> Assets for the different workshops. This is a good entry point for any newcomer that has to learn Atomic Design principles.
