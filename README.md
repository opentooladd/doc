# doc

This repository is used to store all necessary information to work according OpenToolAdd projects process.


* [OpenToolAdd Wiki](https://gitlab.com/opentooladd/doc/-/wikis/OpenToolAdd-Wiki)



## TODO

* [ ] Enhance wiki
* [ ] Define OpenToolAdd guidelines
* [ ] Brainstorm OpenToolAdd doc todo list